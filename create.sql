--  ******ISHOD1********


DROP TABLE DVORANA;
DROP TABLE IGRACI;
DROP TABLE TRENER;
DROP TABLE KLUBOVI;
DROP TABLE KORISNIK;

DROP sequence KORISNIK_ID_SEQ;
DROP sequence KLUBOVI_ID_SEQ;
DROP sequence IGRACI_ID_SEQ;
DROP sequence TRENER_ID_SEQ;
DROP sequence DVORANA_ID_SEQ;



CREATE TABLE Korisnik (
	ID NUMBER(9, 0) NOT NULL,
	JMBAG NUMBER(8, 0) UNIQUE NOT NULL,
	ime VARCHAR2(30) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password VARCHAR2(10) NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
	constraint KORISNIK_PK PRIMARY KEY (ID));

CREATE sequence KORISNIK_ID_SEQ;

CREATE trigger BI_KORISNIK_ID
  before insert on Korisnik
  for each row
begin
  select KORISNIK_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Klubovi (
	ID NUMBER(9, 0) NOT NULL,
	IDkorisnik NUMBER(9, 0) NOT NULL,
	klub VARCHAR2(30) NOT NULL,
	constraint KLUBOVI_PK PRIMARY KEY (ID));

CREATE sequence KLUBOVI_ID_SEQ;

CREATE trigger BI_KLUBOVI_ID
  before insert on Klubovi
  for each row
begin
  select KLUBOVI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Igraci (
	ID NUMBER(9, 0) NOT NULL,
	IDklub NUMBER(9, 0) NOT NULL,
	IDtrener NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(30) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	godine NUMBER(9, 0) NOT NULL,
	pozicija VARCHAR2(30) NOT NULL,
	broj NUMBER(9, 0) NOT NULL,
	broj_koseva NUMBER(9, 0) NOT NULL,
	odigranih_utakmica NUMBER(9, 0) NOT NULL,
	constraint IGRACI_PK PRIMARY KEY (ID));

CREATE sequence IGRACI_ID_SEQ;

CREATE trigger BI_IGRACI_ID
  before insert on Igraci
  for each row
begin
  select IGRACI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Trener (
	ID NUMBER(9, 0) NOT NULL,
	IDklub NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(30) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	godine NUMBER(9, 0) NOT NULL,
	constraint TRENER_PK PRIMARY KEY (ID));

CREATE sequence TRENER_ID_SEQ;

CREATE trigger BI_TRENER_ID
  before insert on Trener
  for each row
begin
  select TRENER_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Dvorana (
	ID NUMBER(9, 0) NOT NULL,
	IDklub NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(30) NOT NULL,
	kapacitet NUMBER(9, 0) NOT NULL,
	adresa VARCHAR2(50) NOT NULL,
	constraint DVORANA_PK PRIMARY KEY (ID));

CREATE sequence DVORANA_ID_SEQ;

CREATE trigger BI_DVORANA_ID
  before insert on Dvorana
  for each row
begin
  select DVORANA_ID_SEQ.nextval into :NEW.ID from dual;
end;

/


/


/

ALTER TABLE Klubovi ADD CONSTRAINT Klubovi_fk0 FOREIGN KEY (IDkorisnik) REFERENCES Korisnik(ID);

ALTER TABLE Igraci ADD CONSTRAINT Igraci_fk0 FOREIGN KEY (IDklub) REFERENCES Klubovi(ID);
ALTER TABLE Igraci ADD CONSTRAINT Igraci_fk1 FOREIGN KEY (IDtrener) REFERENCES Trener(ID);


ALTER TABLE Trener ADD CONSTRAINT Trener_fk0 FOREIGN KEY (IDklub) REFERENCES Klubovi(ID);


ALTER TABLE Dvorana ADD CONSTRAINT Dvorana_fk0 FOREIGN KEY (IDklub) REFERENCES Klubovi(ID);







--insertanje podataka
--korisnik
insert into KORISNIK (ID, JMBAG, ime, prezime, email, password, spol) values (1, 12344, 'Tomislav', 'Kosak', 'tkosak@gmail.hr', 1111, 0);
insert into KORISNIK (ID, JMBAG, ime, prezime, email, password, spol) values (1, 12345, 'Marko', 'Markovic', 'mmarkovic@gmail.hr', 2222, 0);




--klubovi
insert into KLUBOVI (ID, IDkorisnik, klub) values (1, 1, 'Chicago Bulls');







--dvorana
insert into DVORANA (ID, IDklub, ime, kapacitet, adresa) values (1, 1, 'The Garden', 50000, 'NYC');







--trener
insert into TRENER (ID, IDklub, ime, prezime, godine) values (1, 1, 'm', 'm', 57);





--igraci
insert into IGRACI (ID, IDklub, IDtrener, ime, prezime, godine, pozicija, broj, broj_koseva, odigranih_utakmica) 
  values 
(1, 1, 1, 'm', 'm', 33, 'napadac', 10, 706, 857);







--select * from igraci;

--select * from klubovi
-- where
-- ID = 1

